import sys


def read_from_file(name="data.txt"):
    data = []
    try:
        with open(name, "r") as i:
            for line in i:
                data.append(int(line))
    except Exception:
        sys.exit("Cannot read data from file")
    return data


def addition(numbers: list):
    total = 0
    for n in numbers:
        total += n
    return total


def multiplication(numbers: list):
    total = 1
    try:
        for n in numbers:
            total *= n
    except Exception as err:
        print(err)
    return total


def find_min(numbers: list):
    m = numbers[0]
    for i in numbers:
        if i < m:
            m = i
    return m


def find_max(numbers: list):
    m = numbers[0]
    for i in numbers:
        if i > m:
            m = i
    return m

