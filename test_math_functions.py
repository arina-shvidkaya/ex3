from datetime import datetime
import pytest
from math_functions import *
from main import main
import math


def test_addition():
    data = read_from_file()
    assert addition(data) == sum(data)


def test_multiplication():
    data = read_from_file()
    assert math.prod(data) == multiplication(data)


def test_find_min():
    data = read_from_file()
    assert min(data) == find_min(data)


def test_find_max():
    data = read_from_file()
    assert max(data) == find_max(data)


# Тестируем, что чтение из файла не выполнится, если формат файла будет неверный
def test_error_read_from_file():
    with pytest.raises(SystemExit):
        assert read_from_file("test_data.txt")


def test_speed_data():
    data = read_from_file()
    f_data = data[:5]

    from_time_1 = datetime.now().timestamp()
    main(f_data)
    to_time_1 = datetime.now().timestamp()
    time_1 = to_time_1 - from_time_1

    from_time_2 = datetime.now().timestamp()
    main(data)
    to_time_2 = datetime.now().timestamp()
    time_2 = to_time_2 - from_time_2

    assert time_2 >= time_1

