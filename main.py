from math_functions import *


def main(data: list):
    print("Список чисел: " + " ".join(map(str, data)))
    print(f'Сумма чисел в файле: {addition(data)}')
    print(f'Произведение чисел в файле: {multiplication(data)}')
    print(f'Минимальное число в файле: {find_min(data)}')
    print(f'Максимальное число в файле: {find_max(data)}')


if __name__ == "__main__":
    numbers = read_from_file()
    main(numbers)
